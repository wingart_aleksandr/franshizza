$(function(){
// IPad/IPhone
  var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
  ua = navigator.userAgent,

  gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

  scaleFix = function () {
    if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
      viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
      document.addEventListener("gesturestart", gestureStart, false);
    }
  };

  scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

 $(window).load(function(){

// меняем позицию блоков
  function blockPosition(){
    var bodyWidth = $(window).width();
    if(bodyWidth <= 1150 && $('body').hasClass('body_resp')){
      $('.resp_l').prepend($('.fransearch'));
      $('.rblock1').append($('.news_list'));
      $('body').removeClass('body_resp');
    }
    else if(bodyWidth > 1150 && !$('body').hasClass('body_resp')){
      $('.rblock1').prepend($('.fransearch'));
      $(".action").after($('.news_list'));
      $('body').addClass('body_resp');
      // console.log(1);
    }
   }
   blockPosition();

   // $(window).on('resize',blockPosition);


  function blockPosition1(){
    var bodyWidth = $(window).width();
    if(bodyWidth <= 768 && $('body').hasClass('body_resp1')){
      $('.rblock1').append($('.feednews'));
      $('.rblock1').append($('.action'));
      $('body').removeClass('body_resp1');
    }
    else if(bodyWidth > 768 && !$('body').hasClass('body_resp1')){
      $(".fransearch").after($('.action'));
      $(".resp_l").append($('.feednews'));
      $('body').addClass('body_resp1');
      // console.log(2);
    }
   }
   blockPosition1();

   function blockPosition2(){
    var bodyWidth = $(window).width();
    if(bodyWidth <= 320 && $('body').hasClass('body_fr_news')){
      $('.doplinks_after_scr').after($('.step_main_block'));
      $('body').removeClass('body_fr_news');
    }
    else if(bodyWidth > 320 && !$('body').hasClass('body_fr_news')){
      $(".rblock_news").prepend($('.step_main_block'));
      $('body').addClass('body_fr_news');
    }
   }
   blockPosition2();

    // if($('.body_fr_yelki_script').length){
      function blockPosition3(){
      var bodyWidth = $(window).width();
        if(bodyWidth <= 768 && $('body').hasClass('body_fr_yelki_script')){
          $('.fr_info').prepend($('.shinfo'));
          $('body').removeClass('body_fr_yelki_script');
        }
        else if(bodyWidth > 768 && !$('body').hasClass('body_fr_yelki_script')){
          $(".image.im1").after($('.shinfo'));
          $('body').addClass('body_fr_yelki_script');
        }
      }
      blockPosition3();
    // }


   $(window).on('resize',function(){
    setTimeout(function(){
      // blockPosition1();
      blockPosition();
      blockPosition2();
      blockPosition3();
    }, 500);
   });
// позицию блоков end

    //---------------------Tabs-----------
    if ($('#tabs').length){
      $( "#tabs" ).easyResponsiveTabs();
    }
  /*-----Выпадашка------*/
    if($('.threeblock_one_text').length){
      $('.threeblock_one_text').on('click', function(){
        $(this)
          .toggleClass('active')
          .next('.down')
          .slideToggle()
          .parents(".fransearch_itembox")
          .siblings(".fransearch_itembox")
          .find(".threeblock_one_text")
          .removeClass("active")
          .next(".down")
          .slideUp();
      });
    }

     

      

      if ($("#sticker1").length) {
        function sticker(){
          var windowscrollTop = $(window).scrollTop(),
              position1 = $("#sticker1").offset().top,
              position2 = $("#position2").offset().top;
          if($(window).width() < 768){
            if(position1 < windowscrollTop && windowscrollTop < (position2 - 77)){
              $("#sticker1 > div").addClass("sticker").show();
              $(".btn_fixed").removeClass("top_zero")
            }
            else if(windowscrollTop > (position2 - 77)){
              $("#sticker1 > div").hide();
              $(".btn_fixed").addClass("top_zero")
            }
            else{
              $("#sticker1 > div").removeClass("sticker");
              
            }
          }

        }
        sticker();
        $(window).on("scroll", sticker);
      };
      // sticky
      if($(".stickybtn").length){
        $(".stickybtn").sticky({topSpacing:77});  
      }
      // появляеться кнопка (якорь) когда доходит до определенного блока
      if($(".top_anchor").length){
        function upBtn(){
          var windowScroll = $(window).scrollTop(),
              offset = $('.arr_anchor_it').offset().top;
          if(windowScroll>offset){
            $(".top_anchor").fadeIn();
          }else{
            $(".top_anchor").fadeOut();
          }
        }
        upBtn();
        $(window).on('scroll',upBtn);
      }
      
 
        // anchor

        $(".top_anchor_btn").on("click", function(){
            $(".top_anchor_btn").addClass("active");

          if($(this).hasClass("first_click")){
              var elementClick = $(this).attr("href"),
                  destination = $(elementClick).offset().top;
              $('html,body').animate( { scrollTop: destination }, 1100 );
              $(this).removeClass("first_click");
              return false;
          }
          else{
              var destination2 = $("#position2").offset().top;
              $('html,body').animate( { scrollTop: destination2 }, 1100 );
              return false;
          }
        });
// ______________________________________
                if($("#stickybtn_2").length){
                  $("#stickybtn_2").sticky({
                    topSpacing:0,
                    wrapperClassName:"sticky-wrapper2"
                  });
                }

              if($(".top_anchor_btn2").length){
                $(".top_anchor_btn2").on("click", function(){
                  $(".top_anchor_btn2").addClass("active");

                  if($(this).hasClass("first_clickIt")){
                      var elementClick1 = $(this).attr("href"),
                          destinationItem = $(elementClick1).offset().top;
                      $('html,body').animate( { scrollTop: destinationItem }, 1100 );
                      $(this).removeClass("first_clickIt");
                      return false;
                  }
                  else{
                      var destinationItem2 = $("#position22").offset().top;
                      $('html,body').animate( { scrollTop: destinationItem2 }, 1100 );
                      return false;
                  }
                });  
              }
              


// ______________________________________

        $(".top_anchor").on("click", function(){
              var elementClick = $(this).attr("href"),
                  destination = $(elementClick).offset().top;
              $('html,body').animate( { scrollTop: destination }, 1100 );
              return false;
        });

        // anchor item страница отзывы

        if($(".anchor_list").length){
          $(".anchor_list").on("click", function(){
                var elementClick = $(this).attr("href"),
                    destination = $(elementClick).offset().top;
                $('html,body').animate( { scrollTop: destination }, 1100 );
                return false;
          });  
        }
        

  /*-----placeholder in input_320------*/
  // var windowWidth = $(window).width();
  // function labelFocus(){
  //   if(windowWidth < 660){

  //     $(".flblock label,.feednews label").each(function(){
  //       var currentText = $(this).find(".lab span,.feed_span").text(),
  //           currentInp  = $(this).find(".sinp,.fninp");
  //         $(currentInp).attr("placeholder", currentText);
  //     })
  //   }
  // }

  // labelFocus();
  // $(window).on('resize',labelFocus);




        // $(".header_320_box").on("click", function(){
        //   $("body").toggleClass("show_menu")
        // })

       // $(".header_320_box").on("click", ".toggle_menu_btn", function(){
       //    $("body").removeClass("show_menu");
       //  })
       //  $(".arr_it").on("click", function(){
       //    $("body").addClass("show_menu");
       //  });
       //  if($(window).width() <= 660){
       //    $(document).on("click touchstart", function(event) {
       //      if ($(event.target).closest(".show_menu").length) return;
       //      $("body").removeClass("show_menu");
       //      event.stopPropagation();
       //    })
       //  }




  //----owl-carousel-----
  if($('.owl-car1').length){
    $('.owl-car1').each(function(){
      var $this = $(this);
      if($this.hasClass('full-width')){
        // $(this).owlCarousel({
        //     margin: 10 ,
        //     nav: true ,
        //     mouseDrag:true,
        //     autoHeight : true,
        //     items:1
        // });
        $(this).owlCarousel({
          stopOnHover : true,
          rewindNav:false,
          navigation:true,
          paginationSpeed : 2000,
          goToFirstSpeed : 2000,
          singleItem : true,
          autoHeight : true,
          mouseDrag:true
          // transitionStyle:"fade"
        });
      }
      else{
        var itemA = $(this).attr('data-itemA'),
            itemB = $(this).attr('data-itemB'),
            itemC = $(this).attr('data-itemC'),
            itemD = $(this).attr('data-itemD'),
            itemE = $(this).attr('data-itemE');
        $(this).owlCarousel({
             margin: 10 ,
             nav: true ,
             smartSpeed:1000,
             responsive: {
                0 : {
                     items: itemE
                },
                960 : {
                     items: itemD
                },
                1358 : {
                     items: itemC
                },
                1600 : {
                     items: itemB
                },
                1900 : {
                     items: itemA
                }
            }
        });
      }
    });
  };
  if($(".flexslider").length){
    $('.flexslider').flexslider({
      animation: "slide",
      minItems: 1,
      itemMargin: 10,
      slideshow: false, 
      smoothHeight: true, 
      animationSpeed: 200,
      maxItems: 1
    });
  };
  
        // var $slider   = $('.owl-car1'),

        //   sliderOptions = {
        //       margin: 10 ,
        //       nav: true ,
        //       mouseDrag:true,
        //       autoHeight : true,
        //       items:1
        //   };
                  // медиа-запрос
      // enquire.register("screen and (max-width: 660px)", {
      //     unmatch : function() {

      //         $slider.owlCarousel(sliderOptions);

      //     },
      //     match : function() {

      //         $slider.trigger('destroy.owl.carousel');

      //     }
      // });

  // function owlNews(){
  //   var windowWidth = $(window).width();
  //   if(windowWidth < 660){
  //     $(".owl_carousel_news").owlCarousel({
  //            nav: true ,
  //            mouseDrag:true,
  //            items:1
  //       });
  //   }
  // }
  // owlNews();
  // $(window).on('resize',owlNews);

       

        

       

});


// $( document ).on( "swipeleft swiperight", ".header_320_box", function(e) {
//   $("body").toggleClass("show_menu");
// });
// $( document ).on( "swipeleft swiperight", ".arr_it", function(e) {
//   $("body").toggleClass("show_menu");
// });


