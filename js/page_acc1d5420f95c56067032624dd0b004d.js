
; /* Start:/bitrix/templates/.default/components/bitrix/main.profile/personal/script.js*/
function removeElement(arr, sElement)
{
	var tmp = new Array();
	for (var i = 0; i<arr.length; i++) if (arr[i] != sElement) tmp[tmp.length] = arr[i];
	arr=null;
	arr=new Array();
	for (var i = 0; i<tmp.length; i++) arr[i] = tmp[i];
	tmp = null;
	return arr;
}

function SectionClick(id)
{
	var div = document.getElementById('user_div_'+id);
	if (div.className == "profile-block-hidden")
	{
		opened_sections[opened_sections.length]=id;
	}
	else
	{
		opened_sections = removeElement(opened_sections, id);
	}

	document.cookie = cookie_prefix + "_user_profile_open=" + opened_sections.join(",") + "; expires=Thu, 31 Dec 2020 23:59:59 GMT; path=/;";
	div.className = div.className == 'profile-block-hidden' ? 'profile-block-shown' : 'profile-block-hidden';
}

/* End */
;
; /* Start:/bitrix/components/bitrix/system.field.edit/script.js*/
function addElement(Name, thisButton)
{
	if (document.getElementById('main_' + Name))
	{
		var element = document.getElementById('main_' + Name).getElementsByTagName('div');
		if (element && element.length > 0 && element[0])
		{
			var parentElement = element[0].parentNode; // parent
			parentElement.appendChild(element[element.length-1].cloneNode(true));
		}
	}
	return;
}

function addElementFile(Name, thisButton)
{
	var parentElement = document.getElementById('main_' + Name);
	var clone = document.getElementById('main_add_' + Name);
	if(parentElement && clone)
	{
		clone = clone.cloneNode(true);
		clone.id = '';
		clone.style.display = '';
		parentElement.appendChild(clone);
	}
	return;
}

/* End */
;; /* /bitrix/templates/.default/components/bitrix/main.profile/personal/script.js*/
; /* /bitrix/components/bitrix/system.field.edit/script.js*/
