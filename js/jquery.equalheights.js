(function($){
	$.fn.equalHeights=function(minHeight,maxHeight){
		tallest=(minHeight)?minHeight:0;
		this.each(function(){
			if($(this).height()>tallest){tallest=$(this).height()}
		});
		if((maxHeight)&&tallest>maxHeight) tallest=maxHeight;
		return this.each(function(){$(this).height(tallest)})
	}
})(jQuery)

$(window).load(function(){
	if($(document).width()>280){ //767
		if($(".maxheight").length){$(".maxheight").equalHeights()}
	}
})
$(window).resize(function(){
	$(".maxheight").css({height:'auto'});
	if($(document).width()>280){ //767
		if($(".maxheight").length){$(".maxheight").equalHeights()}
	}
})

$(window).load(function(){
	if($(document).width()>280){ //767
		if($(".maxx").length){$(".maxx").equalHeights()}
	}
})
$(window).resize(function(){
	$(".maxx").css({height:'auto'});
	if($(document).width()>280){ //767
		if($(".maxx").length){$(".maxx").equalHeights()}
	}
})

$(window).load(function(){
	if($(document).width()>280){ //767
		if($(".maxx2").length){$(".maxx2").equalHeights()}
	}
})
$(window).resize(function(){
	$(".maxx2").css({height:'auto'});
	if($(document).width()>280){ //767
		if($(".maxx2").length){$(".maxx2").equalHeights()}
	}
})